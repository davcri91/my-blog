# Soft Blog
Blog about _software development_, _software engineering_, _GNU\linux
operating systems_, _free software_ and other topics for which I have a deep
love as _videogames_.


## Install
You need to have gem (ruby) installed. 

``` bash
pacman -S ruby
```

Then:

``` bash
gem install bundler jekyll
```

if you never installed any gem binary file on your PC, you should see this warning:

```
WARNING:  You don't have /home/daenn/.gem/ruby/2.4.0/bin in your PATH,
# note: ruby version can be different.
```

so let's add it for bash:

```
# add this line in a file sourced by the shell
# for example ~/.bashrc
PATH="$PATH:~/.gem/ruby/2.4.0/bin"

# FOR ZSH
path+=('/home/user/.gem/ruby/2.4.0/bin')
# note: substitute "user" with your username
# note: ZSH doesn't want ~ shell expansions here (at least the last time I checked)
```


Install dependencies: 

```
# run bundler to get the dependencies of this project
cd my_blog/src
bundle
```


## Run the blog
Execute:

```
bundle exec jekyll serve
```

in order to serve also drafts, execute:

```
bundle exec jekyll serve --drafts
```

## Study
Continue the study from here: https://jekyllrb.com/docs/structure/
