---
layout: post
title:  "Installing Matlab on Linux"
date:   2016-12-26 23:00:46 +0100
categories: linux matlab
---

Fuck Matlab, seriously!

Installing *Matlab 2015* on a new GNU\Linux operating system is not trivial
anymore.
In fact, after a recent installation, trying to launch matlab returns this error:
```
./matlab
Matlab/bin/glnxa64/MATLAB: error while loading shared libraries: libncurses.so.5: cannot open shared object file: No such file or directory
```
The problem is that new distros, as Arch Linux, has a new version of `libncurses`
that is not compatible with Matlab 2015.
[In the archlinux wiki](https://wiki.archlinux.org/index.php/Matlab#Installing_from_the_MATLAB_installation_software)
there is a solution to this problem: install the old package from the AUR:

[ncurses5-compat-libs](https://aur.archlinux.org/packages/ncurses5-compat-libs/)

To fix this problem on my machine I had to:
```
gpg --keyserver pgp.mit.edu --recv-keys C52048C0C0748FEE227D47A2702353E0F7E48EDB
yaourt -S ncurses5-compat-libs
```

In case of this error:
```
$ ./install                                                              
Preparing installation files ...                                           
Installing ...                                                             
./install: line 738: /tmp/mathworks_12807/sys/java/jre/glnxa64/jre/bin/java
: Permission denied                                                        
```
run the following command to fix the problem:
```
chmod a+x -R sys/
```
