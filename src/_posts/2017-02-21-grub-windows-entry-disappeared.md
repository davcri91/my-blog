---
layout: post
title:  "GRUB Windows 10 entry disappeared after Antergos installation"
date:   2017-02-21 00:00:00 +0100
categories: qemu
---

# What happened ?
On my new Lenovo Ideapad 300 (2016), I installed Antergos alongside an
existing installation of Kali Linux and Windows 10.

The installation of Antergos run succesfully, but when booting the PC GRUB
shows only Antergos and Kali linux: Windows 10 disappeared.

In this cases, it seems that GRUB's OS-prober didn't run during the latest
installation of the bootloader (on Antergos installation in my case).

# Fix this!
All that is needed to do is to run the latest OS installed and run   
`grub-mkconfig -o /boot/grub/grub.cfg`  
This will re-run OS-Prober and it will find all the operating systems installed
on the PC. All these information are stored in the grub configuration file.
