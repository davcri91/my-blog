---
layout: post
title:  "UAS problem with HDD Bay and Linux"
date:   2017-11-1 00:00:00 +0100
categories: linux HD storage
---

# HD problem

uas => USB Attached SCSI

I have a problem with this module: after some usage, there is an error:
`
 ERROR Transfer event for disabled endpoint or incorrect stream ring
`

It seems a bug in uas.  
This problem seems to trigger only with syncthing while synchronizing.

# Useful linkx
- https://bbs.archlinux.org/viewtopic.php?id=211523
- https://bugzilla.redhat.com/show_bug.cgi?id=1164945


# Solution
I don't rememeber exactly. I should check on my Acer laptop with Ubuntu.
Probably is the option added to the kernel:
"options usb-storage quirks=vid:pid:u"
