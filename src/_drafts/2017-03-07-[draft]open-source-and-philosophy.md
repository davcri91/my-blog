---
layout: post
title:  "Open Source and Philosophy"
# date:   2017-03-7 00:00:00 +0100
categories:
---

# The god of FOSS
Richard Stallman in person says that "open source is about spirituality".
:

## Philosophy inspiration
From [Jiddu Krishnamurti](https://it.wikipedia.org/wiki/Jiddu_Krishnamurti):

> "Ciascuno cambi se stesso per cambiare il mondo"

> "Non serve dare risposte, ma spronare gli uomini alla ricerca della verità"

As many things in our existence, good concepts are universal. A fantastic quote from Krishnamurti is applicable to a specific context such as software development.

## Pratical tips
- [Making Night in the Woods Better with Open Source](
https://www.youtube.com/watch?v=Qsiu-zzDYww) by GDC.
- [Open Source Guide](https://opensource.guide/legal/)

## Inspiration Sources
[Lunduke Hour with Stallman](https://www.youtube.com/watch?v=S0y0oXU8YNk)
[GNU website](https://www.gnu.org/)
