---
layout: post
title:  "Storia di uno sbattezzo"
date:   2017-09-18 00:00:00 +0100
categories: life 
---

> **Dal 1948 la Costituzione Repubblicana garantisce, nell'articolo 3,
l'uguaglianza degli individui a prescindere dalla religione**, il che rappresenta
l'abolizione della religione di Stato in Italia, cui si giunse ufficialmente
con la revisione dei Patti Lateranensi del 1984 (Protocollo addizionale, punto
1) e con la sentenza 203/1989 della Corte Costituzionale, che sancisce che la
laicità è il principio supremo dello Stato, abolendo così la religione di
Stato.


