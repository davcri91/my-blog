---
layout: post
title:  "The Great Opportunity: Ubuntu switch to Gnome"
date:   2017-04-11 00:00:00 +0100
categories: linux
---

This turn of events is a great opportunity.

- __Unify__ the forces and reduce fragmentation
- System76 will help Ubuntu and Gnome projects
- Ubuntu, Fedora and other distros will have the same consisten graphical
  interface
- Many tutorials will use the same graphical interface => it will be
  easier to write tutorials and videos on youtube => new users will not be
  scared by the many UI's
- Gnome is probably the healthier project and Ubuntu will bring a great number
of users AND companies that ships PCs with Ubuntu pre-installed (System76 and
  Dell to name a few)
- Advantages for other projects based on Gnome Shell tecnologies: think about
  Linux Mint's Cinnamon, elemantaryOS, ecc.
- Hardware vendors can focus on Wayland. Canonical will not mantain Mir. =>
  more possibilities of success for good GPU drivers on a modern display server


## Immediate improvements
- Wayland by default, it's the real deal!
- One of the firsts immediate improvements we will see is GPU switching per
application by simply right-clicking on an app launcher.

## Gnome/Ubuntu future
- GTK4, scene kit and Vulkan rendered UI
- New control center
