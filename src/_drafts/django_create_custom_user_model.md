---
layout: post
title:  "Django: use a custom user model"
# date:   2017-03-13 16:15:00 +0100
categories: software_development django software_framework
---

# Why user a custom user model ?
Flexibility and the ability to respond to change in an agile way.  
You'll never know when you'll need to extend a User model functionalities, so
it's better to be prepared.

In addition Django itself suggest to use it:
https://docs.djangoproject.com/en/1.10/topics/auth/customizing/#using-a-custom-user-model-when-starting-a-project

> If you’re starting a new project, it’s highly recommended to set up a custom
user model, even if the default User model is sufficient for you. This model
behaves identically to the default user model, but you’ll be able to customize
it in the future if the need arises:

# The problem
Although it is a very important task, Django's documentation (at the current of
writing the stable version is _1.11_) does not provide a very in-depth guide on
how to use a __custom User model__ in our projects, especially if you want to
put models in a separate folder as I usually do.  
Doing what written in the previous link, is not sufficient.

# The solution
We'll break the solution in various steps:
- Creating the User model
- Updating `settings.py`
- [I think this is optional: only in case of models put in a separate folder]
Update `__init__.py` with an import statement
