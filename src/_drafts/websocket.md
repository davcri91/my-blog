---
layout: post
title:  "Ju-Gio-Cu: meeting Websockets"
# date:   2017-04-23 16:00:00 +0100
categories: software_development  
---

# The Problem

Once written all the documentation for Ju-Gio-Cu, we started the setup of our
technologies of choice: _Phaser_ for the web-browser client and _Django_ for the
server side.  

Now we need to exchange data between them:
- The client needs to
- The server needs to

In example:
...

# The solution
Socket for javascript on the web (JS running in a browser environment):
__WebSocket__

> This is made possible by providing a standardized way for the server to send
content to the browser without being solicited by the client, and allowing for
messages to be passed back and forth while keeping the connection open. In this
way, a two-way (bi-directional) ongoing conversation can take place between a
browser and the server.

Other interesting readings about WebSockest:
- https://www.html5rocks.com/en/tutorials/websockets/basics/

> Use Cases Use WebSocket whenever you need a truly low latency, near realtime
connection between the client and the server. Keep in mind that this might
involve rethinking how you build your server side applications with a new focus
on technologies such as event queues. Some example use cases are: Multiplayer
online games Chat applications Live sports ticker Realtime updating social
streams

So WebSockets are the best technology for Ju-Gio-Cu!


A possible solution is to write a WebSocket layer in Python that integrates
with the Django stack (there are libraries to use WebSockets in Python for both
client and server) but there is a better solution, explained also during
the PyCon 2016 in Canada: https://www.youtube.com/watch?v=Dv1bpmYV0vU

__Django Channels__
> Channels are the best thing that happened to Django since Django!

- [Andrew Godwin, "Django, Channels, and Distributed Systems", PyBay2016](https://youtu.be/lw9NA5Ea5wA)
