---
layout: post
title:  "Javascript, module imports and tools"
date:   2017-03-13 16:15:00 +0100
categories: programming js
---

# Ju-Gio-Cu
We're working on Ju-Gio-Cu a project for the Software Engineering course.
The game is an online collectible card game based on the client/server
architecture.  
The client game will run on browsers as the main platform.

# Problems with JS
I'll report directly a very good paragraph found on
[Webpack page](https://webpack.js.org/concepts/modules/):

In modular programming, developers break programs up into discrete chunks of
functionality called a module.
> Each module has a smaller surface area than a full program, making verification,
  debugging, and testing trivial. Well-written modules provide solid abstractions
  and encapsulation boundaries, so that each module has a coherent design and a
  clear purpose within the overall application.  
>
  Node.js has supported modular programming almost since its inception. On the
  web, however, support for modules has been slow to arrive. Multiple tools exist
  that support modular JavaScript on the web, with a variety of benefits and
  limitations. webpack builds on lessons learned from these systems and applies
  the concept of modules to any file in your project.

`require` command will not work in browsers because it is part of
the _npm_ world: it belong to the
[CommonJS](https://en.wikipedia.org/wiki/CommonJS) code.


### Interesting links
- [Introduction to Webpack - Video](https://www.youtube.com/watch?v=9kJVYpOqcVU)
- [Importing JS in another JS file](https://stackoverflow.com/questions/4634644/how-to-include-js-file-in-another-js-file#4634669)
- [ES6 Modules](http://exploringjs.com/es6/ch_modules.html#sec_basics-of-es6-modules)
