---
layout: post
title:  "Useful Git Commands"
# date:   2017-04-23 16:00:00 +0100
categories: software_development git 
---


### Get a changelog like output
```git log --oneline --decorate```

### Delete tags on a remote
[How to delete a git remote tag](https://stackoverflow.com/questions/5480258/how-to-delete-a-git-remote-tag#5480292)
```bash
git push --delete origin tagname
```
