---
layout: post
title:  "atom_editor_useful_commands"
# date:   2017-03-13 16:15:00 +0100
categories: software_development atom
---

## Useful commands installed by default

- AutOflow [CTRL+SHIFT+Q]: break lines at preferred line length, this is useful
to respect a certain line length used in a project.
